var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
const keys = require("../../config/keys");
var User = require("../../models/User");
router.get("/test", (req, res) => res.json({ msg: "User Works" }));

/* GET ALL Customer */
router.get("/", function(req, res, next) {
  User.find(function(err, users) {
    if (err) return next(err);
    res.json(users);
  });
});

/* GET SINGLE Customer BY ID */
router.get("/:id", function(req, res, next) {
  User.findById(req.params.id, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE Customer */
router.post("/", function(req, res, next) {
  console.log("req body", req.body);
  User.create(req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* UPDATE Customer */
router.put("/:id", function(req, res, next) {
  User.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE Customer */
router.delete("/:id", function(req, res, next) {
  User.findByIdAndRemove(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
