import React, { Component } from "react";

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      name: "",
      email: "",
      detail: ""
    };
  }
  handleNameChange = e => {
    e.preventDefault();
    this.setState({ name: e.target.value });
  };

  handleEmailChange = e => {
    e.preventDefault();
    this.setState({ email: e.target.value });
  };

  handleDetailChange = e => {
    e.preventDefault();
    this.setState({ detail: e.target.value });
  };

  handleSubmit(event) {
    event.preventDefault();
    console.log(event.target.value);
    const data = new FormData(event.target);
    console.log(this.state);
    console.log("data", data);
    fetch("/api/customers", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state)
    });
    console.log("props", this.props.handleSubmit1);
    const handleSubmit1 = this.props.handleSubmit1;
    handleSubmit1();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label htmlFor="name">Enter name : </label>

        <input
          id="name"
          name="name"
          type="text"
          onChange={this.handleNameChange}
        /><br/>

        <label htmlFor="email">Enter your email: </label>

        <input
          id="email"
          name="email"
          type="email"
          onChange={this.handleEmailChange}
        /><br/>

        <label htmlFor="detail">Enter your details :</label>

        <input
          id="detail"
          name="detail"
          type="textarea"
          onChange={this.handleDetailChange}
        /><br/>

        <button>Send data!</button>
      </form>
    );
  }
}

export default CustomerForm;
