import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn, InsertButton } from "react-bootstrap-table";
import "../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css";


class CustomerList extends Component {
  createCustomDeleteButton = (onBtnClick) => {
    return (
      <InsertButton onClick={onBtnClick} style={{background: 'red', float: 'right'}}>
          Delete Customer
        </InsertButton>
    );
  }

  handleDelete(event) {
    event.preventDefault();
    const item = this.props.data._id;

    const url = '/api/customers'
    fetch(url +"/" + item, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      }
    });
  }

  render() {
    const options = {
      deleteBtn: this.createCustomDeleteButton
    };
    const selectRow = {
      mode: 'checkbox'
    };

    return (
      <div>
         <BootstrapTable selectRow={ selectRow } options={ options } data= {this.props.data} deleteRow>
          <TableHeaderColumn isKey dataField="name">
            Name
          </TableHeaderColumn>
          <TableHeaderColumn dataField="email">Email</TableHeaderColumn>
          <TableHeaderColumn dataField="detail">Detail</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

export default CustomerList;
