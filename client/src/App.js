import React, { Component } from "react";
import {
  BootstrapTable,
  TableHeaderColumn,
  InsertButton
} from "react-bootstrap-table";
import "./App.css";
import CustomerList from "./components/customerList";
import CustomerForm from "./components/customerForm";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: [],
      showNewCustomerForm: false
    };
    this.handleCustomerFormSubmit = this.handleCustomerFormSubmit.bind(this);
  }

  handleAddCustomerForm = e => {
    e.preventDefault();
    this.setState({ showNewCustomerForm: true });
  };

  handleCustomerFormSubmit() {
    this.setState({
      showNewCustomerForm: false,
      customers: []
    });
    fetch("/api/customers")
      .then(res => res.json())
      .then(customers => {
        this.setState({ customers });
        console.log(
          this.state.customers.map(customer => console.log(customer))
        );
      });
  }

  componentDidMount() {
    fetch("/api/customers")
      .then(res => res.json())
      .then(customers => {
        this.setState({ customers });
        console.log(
          this.state.customers.map(customer => console.log(customer))
        );
      });
  }
  render() {
    return (
      <div className="App">
        <p className="Table-header">Customers List</p>
        <CustomerList data={this.state.customers} />
        <InsertButton onClick={this.handleAddCustomerForm}>
          Add Customer
        </InsertButton>
        {this.state.showNewCustomerForm && (
          <div className="Customer-form">
            <CustomerForm handleSubmit1={this.handleCustomerFormSubmit} />
          </div>
        )}
      </div>
    );
  }
}

export default App;
